package main

import "sync"

type SignalChannel struct {
	C    chan struct{}
	once sync.Once
}

func NewSignalChannel() *SignalChannel {
	return &SignalChannel{C: make(chan struct{})}
}

func (s *SignalChannel) Close() {
	// Making sure that the channel is closed only once.
	s.once.Do(func() {
		close(s.C)
	})
}

func (s *SignalChannel) IsClosed() bool {
	select {
	case <-s.C:
		return true
	default:
		return false
	}
}

func (s *SignalChannel) WaitTillClosed() {
	select {
	case <-s.C:
		return
	}
}
