package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"sync"
	"syscall"
	"time"
)

// Code taken from Electron pcp logger - https://bitbucket.org/sunybingcloud/electron-sps/src/master/pcp/pcp.go
func StartPCPLogging(quit *SignalChannel, wg *sync.WaitGroup, prefix string) {
	defer wg.Done()
	const pcpCommand string = "pmdumptext -m -l -f '' -t 1.0 -d , -c config"
	filename := fmt.Sprintf("%s_%v.pcplog", prefix, time.Now().UnixNano())
	pcpLogFile, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0666)
	pcpLogFileWriter := bufio.NewWriter(pcpLogFile)

	defer func() {
		pcpLogFileWriter.Flush()
		pcpLogFile.Close()
	}()

	cmd := exec.Command("sh", "-c", pcpCommand)
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}

	pipe, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(pipe)

	stopCapping := NewSignalChannel()

	writeLine := func(line string) {
		_, err := pcpLogFileWriter.WriteString(line + "\n")
		if err != nil {
			log.Println("error writing to pcp log file: ", err)
		}
	}

	wg.Add(1)
	go func(done *SignalChannel, wg *sync.WaitGroup) {
		defer wg.Done()
		// Get names of columns
		scanner.Scan()

		// Write to logfile.
		writeLine(scanner.Text())

		// Throw away first set of results.
		scanner.Scan()

		for scanner.Scan() {
			if done.IsClosed() {
				return
			}
			writeLine(scanner.Text())
		}
	}(stopCapping, wg)

	log.Println("PCP logging started")

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	pgid, err := syscall.Getpgid(cmd.Process.Pid)

	quit.WaitTillClosed()
	stopCapping.Close()
	log.Println("PCP logging stopped")

	// http://stackoverflow.com/questions/22470193/why-wont-go-kill-a-child-process-correctly
	// kill process and all children processes
	syscall.Kill(-pgid, 15)
	return
}
