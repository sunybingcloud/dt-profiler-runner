package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	taskranker "github.com/pradykaushik/task-ranker"
	"github.com/pradykaushik/task-ranker/datafetcher"
	"github.com/pradykaushik/task-ranker/datafetcher/prometheus"
	"github.com/pradykaushik/task-ranker/entities"
	"github.com/pradykaushik/task-ranker/query"
	"github.com/pradykaushik/task-ranker/strategies"
	"sync"
	"time"
)

var (
	schedule         = flag.String("schedule", "?/5 * * * * *", "task ranker schedule cron format")
	promEndpoint     = flag.String("prom-endpoint", "http://localhost:9090", "prometheus endpoint")
	hostname         = flag.String("hostname", "localhost", "host on which containers are running")
	containerId      = flag.String("containerid", "", "id of docker container to monitor")
	pcpLogFilePrefix = flag.String("pcplogfileprefix", "", "prefix of the pcp log file")
)

type dummyTaskRanksReceiver struct {
	rankedTasks entities.RankedTasks
}

func (r *dummyTaskRanksReceiver) Receive(rankedTasks entities.RankedTasks) {
	r.rankedTasks = rankedTasks
	fmt.Println(rankedTasks)
}

var dummyReceiver *dummyTaskRanksReceiver

func main() {
	flag.Parse()

	var prometheusDataFetcher datafetcher.Interface
	var err error
	var tRanker *taskranker.TaskRanker

	prometheusDataFetcher, err = prometheus.NewDataFetcher(
		prometheus.WithPrometheusEndpoint(*promEndpoint))
	if err != nil {
		fmt.Println(err)
	}

	dummyReceiver = new(dummyTaskRanksReceiver)
	tRanker, err = taskranker.New(
		taskranker.WithDataFetcher(prometheusDataFetcher),
		taskranker.WithSchedule(*schedule),
		taskranker.WithPrometheusScrapeInterval(1*time.Second),
		taskranker.WithStrategyOptions("dT-profile",
			strategies.WithLabelMatchers([]*query.LabelMatcher{
				{Type: query.TaskID, Label: "container_label_task_id", Operator: query.EqualRegex, Value: "dtp.*"},
				{Type: query.TaskHostname, Label: "container_label_task_hostname", Operator: query.Equal, Value: *hostname}}),
			strategies.WithTaskRanksReceiver(dummyReceiver)))

	if err != nil {
		fmt.Println(err)
	} else {
		tRanker.Start()
		doneCh := NewSignalChannel()
		var wg sync.WaitGroup
		wg.Add(1)
		go StartPCPLogging(doneCh, &wg, *pcpLogFilePrefix)

		// Setting client version to 1.40 to avoid issue=https://github.com/docker/cli/issues/2533
		dockerClient, err := client.NewClientWithOpts(client.WithVersion("1.40"))
		if err != nil {
			fmt.Println("failed to instantiate docker client... will need to manually stop profiler: ", err.Error())
		} else {
			waitCh, errCh := dockerClient.ContainerWait(context.TODO(), *containerId, container.WaitConditionNextExit)
			select {
			case err = <-errCh:
				fmt.Println("could not wait on container: ", err)
			case result := <-waitCh:
				fmt.Println("done waiting for container: ", result.StatusCode)
			}

			// Container is no longer running.
			// Stopping pcp logging.
			doneCh.Close()

			// stopping and killing the container.
			timeout := 5 * time.Second
			err = dockerClient.ContainerStop(context.TODO(), *containerId, &timeout)
			if err != nil {
				fmt.Println("failed to stop the container: ", err)
			} else {
				err = dockerClient.ContainerRemove(context.TODO(), *containerId, types.ContainerRemoveOptions{Force: true})
				if err != nil {
					fmt.Println("failed to kill the container: ", err)
				}
			}
		}

		tRanker.Stop()
		wg.Wait()
	}
}
